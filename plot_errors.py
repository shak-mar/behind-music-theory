import matplotlib.pyplot as plt
import numpy as np
from music import *

le, ue = 5, 20

x = np.array(range(le, ue))
y = np.vectorize(lambda n: sos(errors(n)) if tonalities(n) == n else 0)(x)
#y = np.vectorize(lambda n: np.max(py[n:]))(range(ue - le))
yp = None
xi = np.linspace(x[0], x[-1], 1000)
yi = np.interp(xi, x, y, yp)

fig, ax = plt.subplots()
plt.plot(x, y, 'o', xi, yi, '.')
plt.show()
plt.savefig('errors.png')
